import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';
import { Router, Params } from '@angular/router';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {
  items: Array<any>
  item: string;

  displayedColumns: string[] = ['position', 'name', 'age', 'email', 'phone', 'address', 'action'];
  constructor(
    public firebaseService: FirebaseService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.firebaseService.getUser()
      .subscribe(
        arg => {
          this.items = arg
          console.log(arg);
        }
      );
  }

  delete(data) {
    this.firebaseService.deleteUser(data)
  }

}
