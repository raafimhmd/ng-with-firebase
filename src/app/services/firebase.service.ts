import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  constructor(
    public db: AngularFirestore
  ) { }

  createUser(value) {
    return this.db.collection('users').add({
      name: value.name,
      nameToSearch: value.name.toLowerCase(),
      age: parseInt(value.age),
      email: value.email,
      phone: value.phone,
      address: value.address
    })
  }

  getUser() {
    return this.db.collection('users').snapshotChanges();
  }

  deleteUser(data) {
    return this.db.collection('users').doc(data.payload.doc.id).delete();
  }
}
