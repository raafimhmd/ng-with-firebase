// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyBbyogzkLduxv6En16HvnI91QRt7X5LMts",
    authDomain: "ng-crud-fb.firebaseapp.com",
    databaseURL: "https://ng-crud-fb.firebaseio.com",
    projectId: "ng-crud-fb",
    storageBucket: "ng-crud-fb.appspot.com",
    messagingSenderId: "914307560129",
    appId: "1:914307560129:web:aeda6a0316757685"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
